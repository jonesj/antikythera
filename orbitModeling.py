'''
--------------------------------------------------------------------------------
Orbit Modeling Prototype:
--------------------------------------------------------------------------------
How to use this file for the GUI test:
- Import the file by adding "import orbit_modeling_v2" to the header
- Call "Jacob(time)" in a loop and use the outputted list for the GUI
- Refer to the function's documentation for more detail
--------------------------------------------------------------------------------
'''


import math
import datetime

class body:
    def __init__(self):
        mass = 0
        xPos = 0
        yPos = 0
        print("Celstial body added")

class star(body):
    def __init__(self, m):
        self.mass = m
        self.xPos = 0
        self.yPos = 0
        print("Star added")

class planet(body):
    def __init__(self, mass, semiMajorAxis, eccentricity, periapsis, timeToOrbit, maxInclination, longitudeAscendingNode, longitudePerihelion, baseAngle, st): #should also add some starting date to base the planet off of (Use NASA queries)
        self.name = "Default Planet Name"   #This is the id for each planet object. We need this for database stuff
        self.baseDate = datetime.date(2019, 1, 1) # this is the base date for the base angle (used to base the body on real world data)
        self.baseAngle = baseAngle # this is the base angle at said base time
        self.mass = mass
        self.distance = periapsis # should be changed according to base date
        self.xPos = 0 # should be changed as well
        self.yPos = 0 # should be changed as well
        self.zPos = 0
        self.semiMajorAxis = semiMajorAxis
        self.eccentricity = eccentricity
        self.angle = 0
        self.inclination = 0
        self.timeToOrbit = timeToOrbit
        self.maxInclination = maxInclination
        self.longitudeAscendingNode = longitudeAscendingNode
        self.longitudePerihelion = longitudePerihelion
        print("Planet added")

    def simulate_orbit_continuous(self, t): # Function that will determine the position of the planet with an incrementing 't' variable
        '''
        Function that simulates the orbit of the planet incrementally.

        Inputs:
            - The increment 't'
        Outputs :
            - None, but the angle and coordinates of the celestial body are updated
        Uses :
            Can be used in a loop to continuously update the position of the body
        '''

        self.angle = (t * 2 * math.pi) / self.timeToOrbit + self.longitudePerihelion # assuming the periapsis is the base start point
        if self.angle > 2 * math.pi: #correects the angle to be less than a full rotation
            self.angle -= 2 * math.pi
        self.inclination = self.maxInclination * math.sin(self.angle - self.longitudeAscendingNode) # gets the current inclination

        self.distance = (self.semiMajorAxis * (1 - math.pow(self.eccentricity, 2))) / (1 + self.eccentricity * math.cos(self.angle - self.longitudePerihelion)) # gets the distance from the sun
        self.xPos = math.cos(self.angle) * math.cos(self.inclination) * self.distance
        self.yPos = math.sin(self.angle) * math.cos(self.inclination) * self.distance
        self.zPos = math.sin(self.inclination)

    #def simulate_orbit_date(self, month, day, year, ADAC):

class satellite(body): # STILL NEEDS MODIFICATIONS
    def __init__(self, m, d, pl):
        self.mass = m
        self.distance = d
        self.xPos = pl.xPos + d
        self.yPos = pl.yPos
        self.host = pl
        self.angle = 0
        self.circumference = 2 * d * math.pi
        self.timeToOrbit = 27.322 #should be added as argument and not hard coded
        print("Satellite added")

    def simulate_orbit(self, t, tCurrent):
        newDistance = ((t - tCurrent) * self.circumference)/ self.timeToOrbit
        newAngle = (newDistance * 2 *math.pi) / self.circumference
        self.angle = newAngle
        if self.angle > 2 * math.pi:
            self.angle -= 2 * math.pi
        self.xPos = self.host.xPos + math.cos(self.angle) * self.distance
        self.yPos = self.host.yPos + math.sin(self.angle) * self.distance

def Jacob(time):
    '''
    --------------------------------------------------------------------------------
    This is a function to test the GUI:
    - Input the time to simulate the orbit: works well inside a loop when incrementing the time variable
    - Outputs a list containing the earth and mars's 3d coordinates
    - The distances are in Km, which means very large values (we will porbably be moving to astronomical units (AU) later for that reason).
    - The angles are in radians as requested
    --------------------------------------------------------------------------------
    '''
    Sun = star(1.989 * math.pow(10, 30))
    Earth = planet(5.972 * math.pow(10, 24), 149598262.00, 0.017, 147098291.00, 365.25636, 0, -0.196535244, 1.796767421, 1.749518042, Sun)
    Mars = planet(5.972 * math.pow(10, 24), 227943823.5, 0.093, 206655215, 687.0106875, 0.032288591, 0.865308761, 5.865019079, 0.7309438907, Sun)
    Earth.simulate_orbit_continuous(time)
    Mars.simulate_orbit_continuous(time)
    results = [Earth.xPos, Earth.yPos, Earth.zPos, Mars.xPos, Mars.yPos, Mars.zPos]
    return results


'''
Sun = star(1.989 * math.pow(10, 30))
Earth = planet(5.972 * math.pow(10, 24), 149598262.00, 0.017, 147098291.00, 365.25636, 0, -0.196535244, 1.796767421, 1.749518042, Sun)
Mars = planet(5.972 * math.pow(10, 24), 227943823.5, 0.093, 206655215, 687.0106875, 0.032288591, 0.865308761, 5.865019079, 0.7309438907, Sun)

i = 0
while i < 680:
    Mars.simulate_orbit_continuous(i)
    #print(Earth.angle)
    print('(', Mars.distance, ',', Mars.inclination * 180 / math.pi, ',', Mars.angle * 180 / math.pi, ')')
    i += 1
'''
